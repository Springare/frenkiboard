## Frenkiboard

Install:

1. Copy GFX_Button-Folder into Arduino/libraries.

2. Get https://github.com/BesoBerlin/Arduino-German-Keyboard for using the german keyboard layout. Or you have to check the other layouts and set the keycodes/ascii accordingly.

3. Compile and run the Frenkiboard.ino.

This project was developed for a friend of mine. He is physical disabled (is this political correct?) and has to use a touch-keyboard. Because reasons he has to get a cable-connected one. So i made one with an
Arduino Leonardo and an Adafruit 2.8"-Touchscreen (Capacitiv).