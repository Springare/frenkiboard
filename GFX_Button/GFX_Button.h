#ifndef GFX_Button_h
#define GFX_Button_h

#include <Adafruit_ILI9341.h>

class GFX_Button {

 public:
  GFX_Button(void);
  void initButton(Adafruit_ILI9341 *gfx, int16_t x, int16_t y, 
          uint8_t w, uint8_t h, 
          uint8_t textsize);
  void drawButton(boolean inverted = false);
  boolean contains(int16_t x, int16_t y);

  void press(boolean p);
  boolean isPressed();
  boolean justPressed();
  boolean justReleased();
  uint8_t getCharCode();
  void setCharCode(int p, int value);
  void setHighlight(boolean p);
  void setShiftMode (uint8_t p);

 private:
  Adafruit_ILI9341 *_gfx;
  int16_t _x, _y;
  uint16_t _w, _h;
  uint8_t _textsize;
  uint16_t _outlinecolor, _fillcolor, _textcolor;
  uint8_t _charCodes[3];
  boolean _drawHighlight = false;
  uint8_t _shiftMode = 0;

  boolean currstate, laststate;
};

#endif