#include "GFX_Button.h"
#include "Adafruit_ILI9341.h"

GFX_Button::GFX_Button(void) {
   _gfx = 0;
}

void GFX_Button::initButton(Adafruit_ILI9341 *gfx,
					  int16_t x, int16_t y, 
					  uint8_t w, uint8_t h,
					  uint8_t textsize)
{
  _x = x;
  _y = y;
  _w = w;
  _h = h;
  _outlinecolor = 0xEF5D;
  _fillcolor = 0xEF5D;
  _textcolor = 0x39E7;
  _textsize = textsize;
  _gfx = gfx;
  _charCodes[0] = 0;
  _charCodes[1] = 0;
}

 void GFX_Button::drawButton(boolean inverted) {
   uint16_t fill, outline, text;

	if(!_drawHighlight)
	{
	   if (! inverted) {
		 fill = _fillcolor;
		 outline = _outlinecolor;
		 text = _textcolor;
	   } else {
		 fill =  _textcolor;
		 outline = _outlinecolor;
		 text = _fillcolor;
	   }
	}
	else
	{
	   if (! inverted) {
		 fill = 0xBBCF;
		 outline = _outlinecolor;
		 text = _textcolor;
	   } else {
		 fill =  _textcolor;
		 outline = _outlinecolor;
		 text = 0xBBCF;
	   }
	}

   _gfx->fillRoundRect(_x - (_w/2), _y - (_h/2), _w, _h, min(_w,_h)/4, fill);
   _gfx->drawRoundRect(_x - (_w/2), _y - (_h/2), _w, _h, min(_w,_h)/4, outline);
   
   
   _gfx->setCursor(_x - 2*_textsize, _y-4*_textsize);
   _gfx->setTextColor(text);
   _gfx->setTextSize(_textsize);
   _gfx->print(char(_charCodes[_shiftMode]));
 }

boolean GFX_Button::contains(int16_t x, int16_t y) {  
   if ((x < (_x - _w/2)) || (x > (_x + _w/2))) return false;
   if ((y < (_y - _h/2)) || (y > (_y + _h/2))) return false;
   return true;
 }


 void GFX_Button::press(boolean p) {
   laststate = currstate;
   currstate = p;
 }
 
 boolean GFX_Button::isPressed() { return currstate; }
 boolean GFX_Button::justPressed() { return (currstate && !laststate); }
 boolean GFX_Button::justReleased() { return (!currstate && laststate); }
 void GFX_Button::setHighlight(boolean p) { _drawHighlight = p; }

uint8_t GFX_Button::getCharCode() 
{ 
	return _charCodes[_shiftMode]; 
}
 
void GFX_Button::setCharCode(int p, int value)
{
	_charCodes[p] = value;
	if(value>96 && value < 123)
	{
		_charCodes[1] = value - 32;
	}
}
 
 void GFX_Button::setShiftMode(uint8_t p) 
 { 
	_shiftMode = p; 
	if(_charCodes[_shiftMode] == 0)
		_shiftMode = 0; 
}
 
 