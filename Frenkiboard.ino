// Include of needed Libraries
#include <Adafruit_GFX.h>    // Core graphics library
#include <SPI.h>       // this is needed for display
#include <Adafruit_ILI9341.h>
#include <Wire.h>      // this is needed for FT6206
#include <Adafruit_FT6206.h>
#include <GFX_Button.h>   // Custom written Button-Class
#include <Keyboard_DE.h>  // https://github.com/BesoBerlin/Arduino-German-Keyboard for using German Keyboard Layout

// The FT6206 uses hardware I2C (SCL/SDA)
Adafruit_FT6206 ctp = Adafruit_FT6206();

// The display also uses hardware SPI, plus #9 & #10
#define TFT_CS 10
#define TFT_DC 9
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

// Define Button Height and Row-Start for easy designing the UI
#define BUTTON_H 48
#define FIRST_ROW_Y 61
#define SECOND_ROW_Y 111
#define THIRD_ROW_Y 161
#define FOURTH_ROW_Y 211

// Define number of Buttons for easy access
#define BUTTON_COUNT 46

// Global button-array and shiftMod-variable
GFX_Button buttons[BUTTON_COUNT];
int shiftMod = 0;

void setup() 
{
  // Setting up the display
  tft.begin();
  tft.setRotation(3);   // Using the display sideways
  if (! ctp.begin(40)) {  // pass in 'sensitivity' coefficient
    while (1);
  }
  tft.fillScreen(0xC638);
  
  // create buttons
  // The overall layout is a recreation of the samsung-keyboard
  // Numbers
  buttons[0].initButton(&tft,16,20,28,30,2); buttons[0].setCharCode(0,48); buttons[0].setCharCode(1,61); buttons[0].drawButton(); // 0 =
  buttons[1].initButton(&tft,48,20,28,30,2); buttons[1].setCharCode(0,49); buttons[1].setCharCode(1,33); buttons[1].drawButton(); // 1 !
  buttons[2].initButton(&tft,80,20,28,30,2); buttons[2].setCharCode(0,50); buttons[2].setCharCode(1,34); buttons[2].drawButton(); // 2 "
  buttons[3].initButton(&tft,112,20,28,30,2); buttons[3].setCharCode(0,51); buttons[3].setCharCode(1,42); buttons[3].drawButton();  // 3 *
  buttons[4].initButton(&tft,144,20,28,30,2); buttons[4].setCharCode(0,52); buttons[4].setCharCode(1,36); buttons[4].drawButton();  // 4 $
  buttons[5].initButton(&tft,176,20,28,30,2); buttons[5].setCharCode(0,53); buttons[5].setCharCode(1,37); buttons[5].drawButton();  // 5 %
  buttons[6].initButton(&tft,208,20,28,30,2); buttons[6].setCharCode(0,54); buttons[6].setCharCode(1,38); buttons[6].drawButton();  // 6 &
  buttons[7].initButton(&tft,240,20,28,30,2); buttons[7].setCharCode(0,55); buttons[7].setCharCode(1,47); buttons[7].drawButton();  // 7 /
  buttons[8].initButton(&tft,272,20,28,30,2); buttons[8].setCharCode(0,56); buttons[8].setCharCode(1,40); buttons[8].drawButton();  // 8 (
  buttons[9].initButton(&tft,304,20,28,30,2); buttons[9].setCharCode(0,57); buttons[9].setCharCode(1,41); buttons[9].drawButton();  // 9 )
  
  // Letters, all letters have their uppercases added in the button-class, so q means q Q
  // First Row
  buttons[10].initButton(&tft,14,FIRST_ROW_Y,27,BUTTON_H,2); buttons[10].setCharCode(0,113); buttons[10].drawButton();  // q
  buttons[11].initButton(&tft,43,FIRST_ROW_Y,27,BUTTON_H,2); buttons[11].setCharCode(0,119); buttons[11].drawButton();  // w
  buttons[12].initButton(&tft,72,FIRST_ROW_Y,27,BUTTON_H,2); buttons[12].setCharCode(0,101); buttons[12].drawButton();  // e
  buttons[13].initButton(&tft,101,FIRST_ROW_Y,27,BUTTON_H,2); buttons[13].setCharCode(0,114); buttons[13].drawButton(); // r
  buttons[14].initButton(&tft,130,FIRST_ROW_Y,27,BUTTON_H,2); buttons[14].setCharCode(0,116); buttons[14].drawButton(); // t
  buttons[15].initButton(&tft,159,FIRST_ROW_Y,27,BUTTON_H,2); buttons[15].setCharCode(0,122); buttons[15].drawButton(); // z
  buttons[16].initButton(&tft,188,FIRST_ROW_Y,27,BUTTON_H,2); buttons[16].setCharCode(0,117); buttons[16].drawButton(); // u
  buttons[17].initButton(&tft,217,FIRST_ROW_Y,27,BUTTON_H,2); buttons[17].setCharCode(0,105); buttons[17].drawButton(); // i
  buttons[18].initButton(&tft,246,FIRST_ROW_Y,27,BUTTON_H,2); buttons[18].setCharCode(0,111); buttons[18].drawButton(); // o
  buttons[19].initButton(&tft,275,FIRST_ROW_Y,27,BUTTON_H,2); buttons[19].setCharCode(0,112); buttons[19].drawButton(); // p
  buttons[20].initButton(&tft,304,FIRST_ROW_Y,27,BUTTON_H,2); buttons[20].setCharCode(0,63); buttons[20].setCharCode(1,35); buttons[20].drawButton(); // ? #
  // Second Row  
  buttons[21].initButton(&tft,14,SECOND_ROW_Y,27,BUTTON_H,2); buttons[21].setCharCode(0,97); buttons[21].drawButton(); // a
  buttons[22].initButton(&tft,43,SECOND_ROW_Y,27,BUTTON_H,2); buttons[22].setCharCode(0,115); buttons[22].drawButton(); // s
  buttons[23].initButton(&tft,72,SECOND_ROW_Y,27,BUTTON_H,2); buttons[23].setCharCode(0,100); buttons[23].drawButton(); // d
  buttons[24].initButton(&tft,101,SECOND_ROW_Y,27,BUTTON_H,2); buttons[24].setCharCode(0,102); buttons[24].drawButton(); // f
  buttons[25].initButton(&tft,130,SECOND_ROW_Y,27,BUTTON_H,2); buttons[25].setCharCode(0,103); buttons[25].drawButton(); // g
  buttons[26].initButton(&tft,159,SECOND_ROW_Y,27,BUTTON_H,2); buttons[26].setCharCode(0,104); buttons[26].drawButton(); // h
  buttons[27].initButton(&tft,188,SECOND_ROW_Y,27,BUTTON_H,2); buttons[27].setCharCode(0,106); buttons[27].drawButton(); // j
  buttons[28].initButton(&tft,217,SECOND_ROW_Y,27,BUTTON_H,2); buttons[28].setCharCode(0,107); buttons[28].drawButton(); // k
  buttons[29].initButton(&tft,246,SECOND_ROW_Y,27,BUTTON_H,2); buttons[29].setCharCode(0,108); buttons[29].drawButton(); // l
  buttons[30].initButton(&tft,275,SECOND_ROW_Y,27,BUTTON_H,2); buttons[30].setCharCode(0,64); buttons[30].setCharCode(1,43); buttons[30].drawButton(); // @ +
  buttons[31].initButton(&tft,304,SECOND_ROW_Y,27,BUTTON_H,2); buttons[31].setCharCode(0,39); buttons[31].setCharCode(1,45); buttons[31].drawButton(); // ' -
  // Third Row
  buttons[32].initButton(&tft,22,THIRD_ROW_Y,40,BUTTON_H,2); buttons[32].setCharCode(0,175); buttons[32].drawButton();  // >> means Shift
  buttons[33].initButton(&tft,72,THIRD_ROW_Y,27,BUTTON_H,2); buttons[33].setCharCode(0,121); buttons[33].drawButton();  // y
  buttons[34].initButton(&tft,101,THIRD_ROW_Y,27,BUTTON_H,2); buttons[34].setCharCode(0,120); buttons[34].drawButton(); // x
  buttons[35].initButton(&tft,130,THIRD_ROW_Y,27,BUTTON_H,2); buttons[35].setCharCode(0,99); buttons[35].drawButton();  // c
  buttons[36].initButton(&tft,159,THIRD_ROW_Y,27,BUTTON_H,2); buttons[36].setCharCode(0,118); buttons[36].drawButton(); // v
  buttons[37].initButton(&tft,188,THIRD_ROW_Y,27,BUTTON_H,2); buttons[37].setCharCode(0,98); buttons[37].drawButton();  // b
  buttons[38].initButton(&tft,217,THIRD_ROW_Y,27,BUTTON_H,2); buttons[38].setCharCode(0,110); buttons[38].drawButton(); // n
  buttons[39].initButton(&tft,246,THIRD_ROW_Y,27,BUTTON_H,2); buttons[39].setCharCode(0,109); buttons[39].drawButton(); // m
  buttons[40].initButton(&tft,298,THIRD_ROW_Y,40,BUTTON_H,2); buttons[40].setCharCode(0,174); buttons[40].drawButton(); // << means Backspace
  // Fourth Row
  buttons[41].initButton(&tft,25,FOURTH_ROW_Y,47,BUTTON_H,2); buttons[41].setCharCode(0,95); buttons[41].drawButton();  // _
  buttons[42].initButton(&tft,64,FOURTH_ROW_Y,27,BUTTON_H,2); buttons[42].setCharCode(0,44); buttons[42].setCharCode(1,59); buttons[42].drawButton(); // , ;
  buttons[43].initButton(&tft,160,FOURTH_ROW_Y,161,BUTTON_H,2); buttons[43].setCharCode(0,32); buttons[43].drawButton(); // Spacebar
  buttons[44].initButton(&tft,256,FOURTH_ROW_Y,27,BUTTON_H,2); buttons[44].setCharCode(0,46); buttons[44].setCharCode(1,58); buttons[44].drawButton(); // . :
  buttons[45].initButton(&tft,295,FOURTH_ROW_Y,46,BUTTON_H,2); buttons[45].setCharCode(0,188); buttons[45].drawButton(); // Enterkey

  Keyboard.begin();   // start Keyboard-functionality once, it is needed for main functionality
  }

void loop() // The mainloop 
{
  if (! ctp.touched()) { // dont do anything, until a touch is detected
    return;
  }
  
  TS_Point p = ctp.getPoint();  // create a TouchScreenPoint from the touched point

  int oldx = p.x; // cache the old x
  p.x = map(p.y, 0, 320, 0, 320); // rotate the touched point (the display was rotated prior but the touchscreen was not
  p.y = map(oldx, 0, 240, 240, 0);  // remap y-coordinate

  for (uint8_t b = 0; b < BUTTON_COUNT; b++) {  // for all buttons ...
    if (buttons[b].contains(p.x, p.y)) {  // check if it contains the coordinates
      p.x = 0;  // debounce the coordinates so that no other button will be pressed
      p.y = 0;
      
      buttons[b].drawButton(true);  // draw invert
      if(buttons[b].getCharCode() == 174)   // Backspace-Key
        Keyboard.write(8);
      else if(buttons[b].getCharCode() == 188)  // Enter-Key
        Keyboard.write(176);
      else if(buttons[b].getCharCode() == 175)  // Shift-Key
      {
        shiftMod++;
        shiftMod = shiftMod % 3;
        /*
         * cycle through shiftmode
         * 0 = normal mode
         * 1 = shift mode for one keypress
         * 2 = shift mode until shiftkey is pressed again
        */
        if(shiftMod != 0) // not normal-mode
        {
          buttons[b].setHighlight(true);    // show that shiftmode is active
          for (uint8_t b=0; b<BUTTON_COUNT; b++) {
              buttons[b].setShiftMode(1);   // let the buttons know that shiftmode 1 is active. They show alternate text (mostly Uppercase letters)
              buttons[b].drawButton();  // redraw with new text
          }
        }
        else  // normal-mode
        {
          buttons[b].setHighlight(false);   // shift-key deactivated
          for (uint8_t b=0; b<BUTTON_COUNT; b++) {
              buttons[b].setShiftMode(0);   // set shiftmode back for all buttons
              buttons[b].drawButton();  // redraw with new text
          }
        }
      }
      else  // every other button on the keyboard
      {
          Keyboard.write(buttons[b].getCharCode()); // write the shown character from the pressed button
          if(shiftMod == 1) // reset shift-mode, one character was written
          {
            shiftMod = 0;
            buttons[32].setHighlight(false);    // reset shiftmode like some lines above
            for (uint8_t b=0; b<BUTTON_COUNT; b++) {
                buttons[b].setShiftMode(0);
                buttons[b].drawButton();
            }
          }  
      }
            
      delay(150); // UI debouncing
      buttons[b].drawButton();  // draw button unpressed again
    }
  }
}
